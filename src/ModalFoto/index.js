import { Button, Image, Input, Modal, notification } from "antd";
import React, { Fragment, useState } from "react";
import { postData } from "../utils/fetchData";

const ModalFoto = (props) => {
  const { stateModal, setStateModal, stateProfile, getDataProfile, condition } =
    props;
  const [loading, setLoading] = useState(false);
  const [chooseIndex, setChooseIndex] = useState(null);
  const handleChoose = (e) => {
    let datas = new FormData();
    datas.append("id", e.item.id);
    let access_token = sessionStorage.getItem("token");
    postData("/api/v1/uploads/profile/default", datas, access_token)
      .then((res) => {
        setChooseIndex(e.index);
        getDataProfile();
        notification["success"]({
          message: "Sukses",
          description: res.data.data,
        });
      })
      .catch((e) => {
        notification["error"]({
          message: "Gagal Memilih",
          description: e?.response?.data.data,
        });
      });
  };

  const handleUpload = (e) => {
    setLoading(true);
    if (condition === "FotoProfile") {
      let access_token = sessionStorage.getItem("token");
      let datas = new FormData();
      datas.append("image", e.target.files[0]);
      postData("/api/v1/uploads/profile", datas, access_token)
        .then((res) => {
          setLoading(false);
          getDataProfile();
          notification["success"]({
            message: "Sukses",
            description: "Berhasil Diupload",
          });
        })
        .catch((e) => {
          setLoading(false);
        });
    }
    if (condition === "FotoProfileCover") {
      let access_token = sessionStorage.getItem("token");
      let datas = new FormData();
      datas.append("image", e.target.files[0]);
      postData("/api/v1/uploads/cover", datas, access_token)
        .then((res) => {
          setLoading(false);
          setStateModal(false);
          getDataProfile();
          notification["success"]({
            message: "Sukses",
            description: "Berhasil Diupload",
          });
        })
        .catch((e) => {
          setLoading(false);
        });
    }
  };
  const handleCancel = () => {
    setStateModal(false);
  };
  return (
    <Fragment>
      <Modal
        title="Foto"
        visible={stateModal}
        onCancel={handleCancel}
        width={condition === "FotoProfile" ? 1000 : 500}
        footer={null}
      >
        <div className="flex">
          <Button
            className="cursor-pointer w-1/3 mb-3 ml-3 btnActive"
            onClick={() => document.getElementById("upload").click()}
            style={{
              backgroundColor: loading ? "ff7875" : "#cf1322",
              color: "#FFF",
            }}
            disabled={loading}
          >
            {loading ? "Loading.." : "Upload"}
          </Button>
          <Input
            type="file"
            id={"upload"}
            hidden
            onChange={handleUpload}
            accept="image/jpeg"
          />
        </div>
        {condition === "FotoProfile" && (
          <div>
            {stateProfile &&
              stateProfile.user_pictures.map((item, index) => (
                <>
                  <Image
                    key={index}
                    src={item.picture.url}
                    width={180}
                    preview={false}
                    id={index}
                    className="cursor-pointer m-3"
                    onClick={() => handleChoose({ item, index })}
                    style={{ borderRadius: chooseIndex === index ? 100 : 0 }}
                  />{" "}
                  &nbsp;
                </>
              ))}
          </div>
        )}
      </Modal>
    </Fragment>
  );
};

export default ModalFoto;
