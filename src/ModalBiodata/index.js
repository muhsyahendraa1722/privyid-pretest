import {
  Input,
  Modal,
  Form,
  notification,
  Select,
  DatePicker,
  Button,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import moment from "moment";
import React, { Fragment, useEffect, useState } from "react";
import { postData } from "../utils/fetchData";

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};
const { Option } = Select;
const ModalScreen = (props) => {
  const { stateModal, setStateModal, condition, stateProfile, getDataProfile } =
    props;
  const [stateLoading, setStateLoading] = useState(false);
  const handleOk = () => {
    setStateModal(false);
  };

  const handleCancel = () => {
    setStateModal(false);
  };

  const handleSave = (e) => {
    setStateLoading(true);
    if (condition === "bio") {
      let datePick = moment(e.birthday).format("DD-MM-YYYY");
      let access_token = sessionStorage.getItem("token");
      let datas = new FormData();
      datas.append("name", e.nama);
      datas.append("birthday", datePick);
      datas.append("gender", e.gender);
      datas.append("hometown", e.hometown);
      datas.append("bio", e.bio);
      postData("/api/v1/profile", datas, access_token)
        .then((res) => {
          notification["success"]({
            message: "Sukses",
            description: "Berhasil DiUpdate",
          });
          setStateModal(false);
          setStateLoading(false);
          getDataProfile();
        })
        .catch((e) => {
          setStateLoading(false);
          notification["error"]({
            message: "Error",
            description: "Gagal DiUpdate",
          });
        });
    }
    if (condition === "school") {
      let datePick = moment(e.birthday).format("DD-MM-YYYY");
      let access_token = sessionStorage.getItem("token");
      let datas = new FormData();
      datas.append("school_name", e.school_name);
      datas.append("graduation_time", datePick);
      postData("/api/v1/profile/education", datas, access_token)
        .then((res) => {
          notification["success"]({
            message: "Sukses",
            description: "Berhasil Diupdate",
          });
          setStateModal(false);
          setStateLoading(false);
          getDataProfile();
        })
        .catch((e) => {
          setStateLoading(false);
          notification["error"]({
            message: "Error",
            description: "Gagal DiUpdate",
          });
        });
    }
    if (condition === "company") {
      let datePick = moment(e.starting_from).format("DD-MM-YYYY");
      let datePickEnd = moment(e.ending_in).format("DD-MM-YYYY");
      let access_token = sessionStorage.getItem("token");
      let datas = new FormData();
      datas.append("company_name", e.company_name);
      datas.append("position", e.position);
      datas.append("starting_from", datePick);
      datas.append("ending_in", datePickEnd);
      postData("/api/v1/profile/career", datas, access_token)
        .then((res) => {
          setStateLoading(false);
          notification["success"]({
            message: "Sukses",
            description: "Berhasil Diupdate",
          });
          setStateModal(false);
          getDataProfile();
        })
        .catch((e) => {
          setStateLoading(false);
          notification["error"]({
            message: "Error",
            description: "Gagal DiUpdate",
          });
        });
    }
  };
  const [form] = Form.useForm();
  useEffect(() => {
    if (condition === "bio") {
      form.setFieldsValue({
        nama: stateProfile && stateProfile.name,
        birthday: stateProfile && moment(stateProfile.birthday),
        gender: stateProfile && stateProfile.gender === "male" ? "0" : "1",
        bio: stateProfile && stateProfile.bio,
        hometown: stateProfile && stateProfile.hometown,
      });
    }
  }, [condition]);
  useEffect(() => {
    if (condition === "school") {
      form.setFieldsValue({
        school_name: stateProfile && stateProfile.education.school_name,
        graduation_time:
          stateProfile && moment(stateProfile.education.graduation_time),
      });
    }
  }, [condition]);
  useEffect(() => {
    if (condition === "company") {
      form.setFieldsValue({
        company_name: stateProfile && stateProfile.career.company_name,
        starting_from:
          stateProfile && moment(stateProfile.career.starting_from),
        ending_in: stateProfile && moment(stateProfile.career.ending_in),
      });
    }
  }, [condition]);

  return (
    <Fragment>
      <Modal
        title="Update Data"
        visible={stateModal}
        onOk={handleOk}
        onCancel={handleCancel}
        width={800}
        footer={null}
      >
        <Form {...layout} name="Update Data" onFinish={handleSave} form={form}>
          {condition === "school" && (
            <>
              <Form.Item
                label={"Sekolah:"}
                name="school_name"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Sekolah",
                  },
                ]}
              >
                <Input placeholder="Masukkan Sekolah" />
              </Form.Item>
              <Form.Item
                label={"Tanggal Lulus:"}
                name="graduation_time"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Tanggal Lulus",
                  },
                ]}
              >
                <DatePicker format={"DD-MM-YYYY"} />
              </Form.Item>
              <div className="text-right pr-5">
                <Button
                  className="w-20 btnActive"
                  style={{
                    backgroundColor: stateLoading ? "#ff7875" : "#cf1322",
                    color: "#FFF",
                  }}
                  htmlType="submit"
                  disabled={stateLoading}
                >
                  {stateLoading ? "Loading.." : "Edit"}
                </Button>
              </div>
            </>
          )}
          {condition === "company" && (
            <>
              <Form.Item
                label={"Perusahaan:"}
                name="company_name"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Perusahaan",
                  },
                ]}
              >
                <Input
                  defaultValue={
                    stateProfile && stateProfile.career.company_name
                  }
                  placeholder="Masukkan Perusahaan"
                />
              </Form.Item>
              <Form.Item
                label={"Posisi:"}
                name="position"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Posisi",
                  },
                ]}
              >
                <Input placeholder="Masukkan Posisi" />
              </Form.Item>
              <Form.Item
                label={"Tanggal Mulai:"}
                name="starting_from"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Tanggal Mulai Kerja",
                  },
                ]}
              >
                <DatePicker format={"DD-MM-YYYY"} />
              </Form.Item>
              <Form.Item
                label={"Tanggal Terakhir:"}
                name="ending_in"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Tanggal Terakhir Kerja",
                  },
                ]}
              >
                <DatePicker format={"DD-MM-YYYY"} />
              </Form.Item>
              <div className="text-right pr-5">
                <Button
                  className="w-20 btnActive"
                  style={{
                    backgroundColor: stateLoading ? "#ff7875" : "#cf1322",
                    color: "#FFF",
                  }}
                  htmlType="submit"
                  disabled={stateLoading}
                >
                  {stateLoading ? "Loading.." : "Edit"}
                </Button>
              </div>
            </>
          )}
          {condition === "bio" && (
            <>
              <Form.Item
                label={"Nama:"}
                name="nama"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Nama",
                  },
                ]}
              >
                <Input placeholder="Masukkan Nama" />
              </Form.Item>
              <Form.Item
                label={"Jenis Kelamin:"}
                name="gender"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Jenis Kelamin",
                  },
                ]}
              >
                <Select placeholder="Pilih" style={{ width: "100%" }}>
                  <Option value="0">Laki-Laki</Option>
                  <Option value="1">Perempuan</Option>
                </Select>
              </Form.Item>
              <Form.Item
                label={"Tanggal Lahir:"}
                name="birthday"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Tanggal Lahir",
                  },
                ]}
              >
                <DatePicker format={"DD-MM-YYYY"} />
              </Form.Item>
              <Form.Item
                label={"Alamat:"}
                name="hometown"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Alamat",
                  },
                ]}
              >
                <Input placeholder="Masukkan Alamat" />
              </Form.Item>
              <Form.Item
                label={"Biodata:"}
                name="bio"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Biodata",
                  },
                ]}
              >
                <TextArea rows={4} />
              </Form.Item>
              <div className="text-right pr-5">
                <Button
                  className="w-20 btnActive"
                  style={{
                    backgroundColor: stateLoading ? "#ff7875" : "#cf1322",
                    color: "#FFF",
                  }}
                  htmlType="submit"
                  disabled={stateLoading}
                >
                  {stateLoading ? "Loading.." : "Edit"}
                </Button>
              </div>
            </>
          )}
        </Form>
      </Modal>
    </Fragment>
  );
};

export default ModalScreen;
