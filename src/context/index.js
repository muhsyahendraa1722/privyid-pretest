import React, { useReducer } from "react";

export const ApiContext = React.createContext({});
ApiContext.displayName = "ApiContext";

const initialState = {
  loading: true,
  setTab: "1",
  data: {},
};

const reducer = (state, action) => {
  switch (action.type) {
    case "TABS_PINDAH":
      return {
        ...state,
        setTab: action.payload.tabs,
        data: action.payload.datas,
      };
    default:
      return state;
  }
};

const ApiContextProvider = (props) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <ApiContext.Provider value={{ state, dispatch }}>
      {props.children}
    </ApiContext.Provider>
  );
};

export default ApiContextProvider;
