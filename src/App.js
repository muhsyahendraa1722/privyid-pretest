import { Fragment } from "react";
import { BrowserRouter } from "react-router-dom";
import Routes from "./Route";
import "./App.css";
import ApiContextProvider from "./context";

function App() {
  return (
    <Fragment>
      <ApiContextProvider>
        <BrowserRouter>
          <Routes />
        </BrowserRouter>
      </ApiContextProvider>
    </Fragment>
  );
}

export default App;
