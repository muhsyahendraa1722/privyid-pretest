import axios from "axios";

export const postData = (params, data, token) => {
  return axios.post(
    `https://herokuanywhere.herokuapp.com/http://pretest-qa.dcidev.id${params}`,
    data,
    {
      headers: {
        "Access-Control-Allow-Credentials": true,
        "Content-Type": "application/json",
        Authorization: token ? token : null,
      },
    }
  );
};

export const getDatas = (params) => {
  return axios.get(
    `https://herokuanywhere.herokuapp.com/http://pretest-qa.dcidev.id${params}`,
    {
      headers: {
        Accept: "application/json",
      },
    }
  );
};
