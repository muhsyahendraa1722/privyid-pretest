import { Result, Button } from "antd";
import React from "react";
// import Container from "components/Container";

const NotFound = () => (
  //   <Container>
  <Result
    status="warning"
    title="Halaman yang anda cari tidak ditemukan"
    extra={
      <Button ghost type="primary" key="console">
        kembali
      </Button>
    }
  />
  //   </Container>
);
export default NotFound;
