import { Switch, Route, Redirect } from "react-router-dom";
import LoginRegister from "../pages/LoginRegister";
import OtpScreen from "../pages/otp.js";
import DashboardProfile from "../pages/Profile";
import MyProfile from "../pages/Profile/myProfile";
import NotFound from "./NotFound";

const Routes = () => {
  return (
    <Switch>
      <Route render={(props) => <LoginRegister />} path="/Login" />
      <Route render={(props) => <LoginRegister />} path="/Register" />
      <Route render={(props) => <OtpScreen />} path="/otp/:id" />
      <Route render={(props) => <DashboardProfile />} path="/myprofile" />
      <Route render={(props) => <MyProfile />} path="/myprofile" />
      {sessionStorage.getItem("token") ? (
        <Redirect exact from="/" to="/myprofile" />
      ) : (
        <Redirect exact from="/" to="/login" />
      )}
      <Route component={NotFound} />
    </Switch>
  );
};
export default Routes;
