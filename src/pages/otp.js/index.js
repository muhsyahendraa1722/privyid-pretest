import { Button, Input, Statistic } from "antd";
import React, { useEffect, useState } from "react";
import { postData } from "../../utils/fetchData";
import { useHistory, useLocation } from "react-router-dom";
import "./style.css";
import LoginRegisterContainer from "../../container/loginRegisterContainer";

const { Countdown } = Statistic;
const OtpScreen = (props) => {
  const [stateOtp, setStateOtp] = useState("");
  const [stateCouthDown, setStateCouthDown] = useState(false);
  const [stateLoading, setStateLoading] = useState(false);
  const history = useHistory();
  const location = useLocation();

  const deadline = Date.now() + 1 * 5 * 60 * 1000; // Moment is also OK

  const onFinish = () => {
    setStateCouthDown(false);
  };

  const trySend = () => {
    setStateCouthDown(true);
    let phones = location?.state?.user;
    let res = new FormData();
    res.append("phone", phones);
    postData("/api/v1/register/otp/request", res).then((result) => {});
  };
  useEffect(() => {
    if (!location.state) {
      history.push("/register");
    }
  }, [location, history]);
  const sendCode = () => {
    const pathname = window.location.pathname;
    setStateLoading(true);
    let res = new FormData();
    res.append("user_id", pathname.split("/")[2]);
    res.append("otp_code", stateOtp);
    postData("/api/v1/register/otp/match", res)
      .then((res) => {
        const result = res.data.data;
        setStateLoading(false);
        sessionStorage.setItem("token", result.user.access_token);
        history.push("/myprofile");
      })
      .catch((e) => {
        setStateLoading(false);
      });
  };
  const handleInput = (e) => {
    setStateOtp(e.target.value);
  };
  useEffect(() => {
    const obj = document.getElementById("partitioned");
    obj.addEventListener("keydown", stopCarret);
    obj.addEventListener("keyup", stopCarret);

    function stopCarret() {
      if (obj.value.length > 3) {
        setCaretPosition(obj, 3);
      }
    }

    function setCaretPosition(elem, caretPos) {
      if (elem != null) {
        if (elem.createTextRange) {
          const range = elem.createTextRange();
          range.move("character", caretPos);
          range.select();
        } else {
          if (elem.selectionStart) {
            elem.focus();
            elem.setSelectionRange(caretPos, caretPos);
          } else elem.focus();
        }
      }
    }
  }, []);
  return (
    <LoginRegisterContainer>
      <div className="flex justify-center mt-5">
        <div className="text-center text-lg mt-3">Masukkan Kode OTP</div>
      </div>
      <div className="text-center text-xs mt-2">
        Kode OTP telah dikirim ke nomor {location?.state?.user}.<br /> Silahkan
        cek kotak masuk telepon anda.
      </div>
      <div className="flex justify-center">
        <div id="divOuter" className="m-3">
          <div id="divInner">
            <Input
              id="partitioned"
              type="text"
              pattern="[\d]{9}"
              maxLength="4"
              className="p-0"
              onChange={handleInput}
            />
            <div className="mt-5 flex justify-center">
              <Button
                className="w-40 btnActive"
                style={{
                  backgroundColor: stateLoading ? "#ff7875" : "#f5222d",
                  color: "white",
                  borderRadius: 10,
                  height: 35,
                }}
                onClick={() => sendCode()}
                disabled={stateLoading}
              >
                {stateLoading ? "Loading.." : "Verifikasi"}
              </Button>
            </div>
          </div>
        </div>
      </div>
      <div className="mt-3 justify-center flex">
        <div className="block text-center">
          Belum mendapatkan kode OTP ?{" "}
          {stateCouthDown ? (
            <div style={{ pointerEvents: "none", marginLeft: 5 }}>
              <Countdown
                title={null}
                format="mm:ss"
                // format={moment(deadline).format("HH:mm")}
                value={deadline}
                onFinish={onFinish}
                valueStyle={{ fontSize: 13.5 }}
              />
            </div>
          ) : (
            <div
              className="underline font-normal text-blue-500 cursor-pointer ml-1"
              onClick={() => trySend()}
            >
              {" "}
              Kirim Ulang SMS
            </div>
          )}
        </div>
      </div>
      <div className="flex justify-center mt-5">
        <div
          className=" font-semibold text-black-500 cursor-pointer ml-1"
          onClick={() => history.push("/register")}
        >
          Nomor Telepon Salah !
        </div>
      </div>
    </LoginRegisterContainer>
  );
};

export default OtpScreen;
