import { notification, Spin, Tabs } from "antd";
import React, { Fragment, useContext, useEffect, useState } from "react";
import ProfileContainer from "../../container/profileContainer";
import { ApiContext } from "../../context";
import { getDatas } from "../../utils/fetchData";
import Career from "./Career";
import Education from "./Education";
import MyProfile from "./myProfile";
import Profile from "./Profile";
import Upload from "./Upload";
import { useHistory } from "react-router-dom";

const { TabPane } = Tabs;

const DashboardProfile = () => {
  const apiContext = useContext(ApiContext);
  const [stateTabs, setStateTabs] = useState("1");
  const [stateRegister, setStateRegister] = useState(null);
  const [loading, setLoading] = useState(true);
  const history = useHistory();
  const { state, dispatch } = apiContext;
  const { setTab } = state;

  useEffect(() => {
    const pindahTab = () => {
      setStateTabs(setTab);
    };
    pindahTab();
  }, [setTab]);
  const getDataProfile = async () => {
    try {
      const { data } = await getDatas(
        "/api/v1/oauth/credentials?access_token=" +
          sessionStorage.getItem("token")
      );
      const res = data.data;
      setLoading(false);
      const condition = Object.values(res.user).filter((item) => item === null);
      console.log(condition);
      if (condition.length > 3) {
        setStateRegister(false);
      } else {
        setStateRegister(true);
      }
    } catch (e) {
      history.push("/login");
      setLoading(false);
      notification["error"]({
        message: "Error",
        description: "Gagal Validasi Data",
      });
    }
  };
  useEffect(() => {
    getDataProfile();
  }, []);
  return (
    <Fragment>
      <Spin spinning={loading}>
        {stateRegister && <MyProfile />}
        {!stateRegister && (
          <ProfileContainer>
            <Tabs
              defaultActiveKey={"1"}
              tabBarStyle={{ borderBottom: "none" }}
              activeKey={stateTabs}
            >
              <TabPane tab={null} key="1">
                <Profile setStateTabs={setStateTabs} />
              </TabPane>
              <TabPane tab={null} key={"2"}>
                <Education />
              </TabPane>
              <TabPane tab={null} key="3">
                <Career />
              </TabPane>
              <TabPane tab={null} key="4">
                <Upload />
              </TabPane>
            </Tabs>
          </ProfileContainer>
        )}
      </Spin>
    </Fragment>
  );
};

export default DashboardProfile;
