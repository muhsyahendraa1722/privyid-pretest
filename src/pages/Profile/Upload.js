import {
  Button,
  DatePicker,
  Divider,
  Form,
  Image,
  Input,
  notification,
  Select,
} from "antd";
import React, { Fragment, useContext, useEffect, useState } from "react";
import { ApiContext } from "../../context";
import { postData } from "../../utils/fetchData";
import { useHistory } from "react-router-dom";

const Upload = (props) => {
  const [stateUpload, setStateUpload] = useState([]);
  const [loading, setLoading] = useState(false);
  const [chooseIndex, setChooseIndex] = useState(null);
  const apiContext = useContext(ApiContext);
  const { state, dispatch } = apiContext;
  const history = useHistory();
  const handleChoose = (e) => {
    let datas = new FormData();
    datas.append("id", e.item.id);
    let access_token = sessionStorage.getItem("token");
    postData("/api/v1/uploads/profile/default", datas, access_token)
      .then((res) => {
        setChooseIndex(e.index);
        notification["success"]({
          message: "Sukses",
          description: res.data.data,
        });
      })
      .catch((e) => {
        notification["error"]({
          message: "Gagal Memilih",
          description: e?.response?.data.data,
        });
      });
  };

  const handleUpload = (e) => {
    let array = [];
    setLoading(true);
    let access_token = sessionStorage.getItem("token");
    let datas = new FormData();
    datas.append("image", e.target.files[0]);
    postData("/api/v1/uploads/profile", datas, access_token)
      .then((res) => {
        setLoading(false);
        array.push(res.data.data.user_picture);
        setStateUpload((prevstate) => [...prevstate, ...array]);
        notification["success"]({
          message: "Sukses",
          description: "Berhasil Diupload",
        });
      })
      .catch((e) => {
        setLoading(false);
      });
  };

  const handleSave = () => {
    window.location.reload();
  };

  return (
    <Fragment>
      {/* <ProfileContainer> */}
      <div className="pt-3">
        <div className="container mt-8">
          <div className="ml-5 text-xl font-semibold">Upload Foto</div>
          <Divider />
          <div className="grid grid-flow-col grid-cols-2 gap-4 flex justify-around">
            <Button
              className="cursor-pointer w-1/3 mb-3 ml-3 btnActive"
              onClick={() => document.getElementById("upload").click()}
              style={{
                backgroundColor: loading ? "ff7875" : "#cf1322",
                color: "#FFF",
              }}
              disabled={loading}
            >
              {loading ? "Loading.." : "Upload"}
            </Button>
            <Input
              type="file"
              id={"upload"}
              hidden
              // value={NamaFile === "" && NamaFile}
              onChange={handleUpload}
              accept="image/jpeg"
            />
          </div>
          <div>
            {stateUpload.length > 0 &&
              stateUpload.map((item, index) => (
                <>
                  <Image
                    key={index}
                    src={item.picture.url}
                    width={180}
                    preview={false}
                    id={index}
                    className="cursor-pointer m-3"
                    onClick={() => handleChoose({ item, index })}
                    style={{ borderRadius: chooseIndex === index ? 100 : 0 }}
                  />{" "}
                  &nbsp;
                </>
              ))}
          </div>
          <div className="text-right pr-5">
            <Button
              className="w-20 btnActive"
              style={{ backgroundColor: "#cf1322", color: "#FFF" }}
              onClick={handleSave}
            >
              Simpan
            </Button>
          </div>
        </div>
      </div>
      {/* </ProfileContainer> */}
    </Fragment>
  );
};
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 12 },
};

export default Upload;
