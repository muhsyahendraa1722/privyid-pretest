import { Button, Col, Row } from "antd";
import moment from "moment";
import React, { Fragment, useContext, useEffect, useState } from "react";
import ProfileContainer from "../../container/profileContainer";
import ModalScreen from "../../ModalBiodata";
import ModalFoto from "../../ModalFoto";
import { getDatas } from "../../utils/fetchData";

const MyProfile = () => {
  const [stateProfile, setStateProfile] = useState(null);
  const [stateModal, setStateModal] = useState(false);
  const [modalFotoProfil, setModalFotoProfil] = useState(false);
  const [propsEdit, setPropsEdit] = useState("");
  const getDataProfile = async () => {
    // getDatas;
    const { data } = await getDatas(
      "/api/v1/oauth/credentials?access_token=" +
        sessionStorage.getItem("token")
    );
    const res = data.data;
    setStateProfile(res.user);
  };
  useEffect(() => {
    getDataProfile();
  }, []);
  const openModal = (e) => {
    setStateModal(true);
    setPropsEdit(e);
  };
  const openModalFoto = () => {
    setModalFotoProfil(true);
    setPropsEdit("FotoProfile");
  };

  const openModalFotoCover = () => {
    setModalFotoProfil(true);
    setPropsEdit("FotoProfileCover");
  };
  return (
    <Fragment>
      {stateModal && (
        <ModalScreen
          stateModal={stateModal}
          setStateModal={setStateModal}
          condition={propsEdit}
          stateProfile={stateProfile}
          getDataProfile={getDataProfile}
        />
      )}
      {modalFotoProfil && (
        <ModalFoto
          stateModal={modalFotoProfil}
          setStateModal={setModalFotoProfil}
          condition={propsEdit}
          stateProfile={stateProfile}
          getDataProfile={getDataProfile}
        />
      )}
      <ProfileContainer>
        <div className="cursor-pointer" onClick={() => openModalFotoCover()}>
          <img
            src={
              stateProfile && stateProfile.cover_picture.url
                ? stateProfile.cover_picture.url
                : "https://st.depositphotos.com/1239414/3478/i/950/depositphotos_34782863-stock-photo-abstract-background-gray-colour.jpg"
            }
            alt="bg-cover"
            style={{ width: "100%", height: 200 }}
          />
        </div>
        <div className="absolute top-28" style={{ right: 0, left: 150 }}>
          <div
            style={{
              width: 150,
              height: 150,
              backgroundColor: "red",
              borderRadius: 20,
            }}
          >
            <img
              src={
                stateProfile
                  ? stateProfile.user_picture.picture.url
                  : "https://api.duniagames.co.id/api/content/upload/file/8143860661599124172.jpg"
              }
              className="cursor-pointer"
              onClick={() => openModalFoto()}
              style={{
                backgroundSize: "containt",
                borderRadius: 20,
                height: "100%",
              }}
            />
          </div>
        </div>
        <div className="flex ml-12">
          <div className="mt-20">
            <div className="w-56 h-60">
              <div className="flex">
                <div>Nama:</div> &nbsp;
                <div>{stateProfile && stateProfile.name}</div>
              </div>
              <div className="flex">
                <div>TTL:</div> &nbsp;
                <div>
                  {stateProfile &&
                    moment(stateProfile.birthday).format("DD-MM-YYYY")}
                </div>
              </div>
              <div className="flex">
                <div>Jenis Kelamin:</div> &nbsp;
                <div>
                  {stateProfile && stateProfile.gender === "male"
                    ? "Laki-Laki"
                    : stateProfile && stateProfile.gender === "female"
                    ? "Perempuan"
                    : null}
                </div>
              </div>
              <div className="flex">
                <div>Umur:</div> &nbsp;
                <div>{stateProfile && stateProfile.age}</div>
              </div>
              <div>
                <div>Biodata:</div>
                <div>{stateProfile && stateProfile.bio}</div>
              </div>
              <div className="flex justify-center mt-3">
                <Button
                  onClick={() => openModal("bio")}
                  className="btnActive w-40"
                  style={{ backgroundColor: "#f5222d", color: "#FFF" }}
                >
                  Edit
                </Button>
              </div>
            </div>
          </div>
          <Row>
            <Col span={8}>
              <div className="mt-8 ml-10">
                <div className="w-80 h-32">
                  <div className="flex">
                    <div>Perusahaan:</div> &nbsp;
                    <div>
                      {stateProfile && stateProfile.career.company_name}
                    </div>
                  </div>
                  <div className="flex">
                    <div>Start:</div> &nbsp;
                    <div>
                      {stateProfile &&
                        moment(stateProfile.career.starting_from).format(
                          "DD-MM-YYYY"
                        )}
                    </div>
                  </div>
                  <div className="flex">
                    <div>Akhir:</div> &nbsp;
                    <div>
                      {stateProfile &&
                        moment(stateProfile.career.ending_in).format(
                          "DD-MM-YYYY"
                        )}
                    </div>
                  </div>
                </div>
              </div>
              <div className="mt-8 ml-10">
                <div className="w-80 h-32">
                  <div className="flex">
                    <div>Perusahaan:</div> &nbsp;
                    <div>
                      {stateProfile && stateProfile.career.company_name}
                    </div>
                  </div>
                  <div className="flex">
                    <div>Start:</div> &nbsp;
                    <div>
                      {stateProfile &&
                        moment(stateProfile.career.starting_from).format(
                          "DD-MM-YYYY"
                        )}
                    </div>
                  </div>
                  <div className="flex">
                    <div>Akhir:</div> &nbsp;
                    <div>
                      {stateProfile &&
                        moment(stateProfile.career.ending_in).format(
                          "DD-MM-YYYY"
                        )}
                    </div>
                  </div>
                  <div className="flex justify-center mt-3">
                    <Button
                      onClick={() => openModal("company")}
                      className="btnActive w-40"
                      style={{ backgroundColor: "#f5222d", color: "#FFF" }}
                    >
                      Edit
                    </Button>
                  </div>
                </div>
              </div>
            </Col>
            <Col span={8}>
              <div className="mt-8 ml-32">
                <div className="w-80 h-32">
                  <div className="flex">
                    <div>Sekolah:</div> &nbsp;
                    <div>
                      {stateProfile && stateProfile.education.school_name}
                    </div>
                  </div>
                  <div className="flex">
                    <div>Lulus:</div> &nbsp;
                    <div>
                      {stateProfile &&
                        moment(stateProfile.education.graduation_time).format(
                          "DD-MM-YYYY"
                        )}
                    </div>
                  </div>
                </div>
              </div>
              <div className="mt-8 ml-32">
                <div className="w-80 h-32">
                  <div className="flex">
                    <div>Sekolah:</div> &nbsp;
                    <div>
                      {stateProfile && stateProfile.education.school_name}
                    </div>
                  </div>
                  <div className="flex">
                    <div>Lulus:</div> &nbsp;
                    <div>
                      {stateProfile &&
                        moment(stateProfile.education.graduation_time).format(
                          "DD-MM-YYYY"
                        )}
                    </div>
                  </div>
                  <div className="flex justify-center mt-3">
                    <Button
                      onClick={() => openModal("school")}
                      className="btnActive w-40"
                      style={{ backgroundColor: "#f5222d", color: "#FFF" }}
                    >
                      Edit
                    </Button>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </ProfileContainer>
    </Fragment>
  );
};

export default MyProfile;
