import {
  Button,
  DatePicker,
  Divider,
  Form,
  Input,
  notification,
  Select,
} from "antd";
import moment from "moment";
import React, { Fragment, useContext, useState } from "react";
import { ApiContext } from "../../context";
import { postData } from "../../utils/fetchData";

const Career = (props) => {
  const apiContext = useContext(ApiContext);
  const { state, dispatch } = apiContext;
  const handleSave = (e) => {
    // props.setStateTabs("1");
    let datePick = moment(e.starting_from).format("DD-MM-YYYY");
    let datePickEnd = moment(e.ending_in).format("DD-MM-YYYY");
    let access_token = sessionStorage.getItem("token");
    let datas = new FormData();
    datas.append("company_name", e.company_name);
    datas.append("position", e.position);
    datas.append("starting_from", datePick);
    datas.append("ending_in", datePickEnd);
    postData("/api/v1/profile/career", datas, access_token).then((res) => {
      notification["success"]({
        message: "Sukses",
        description: "Berhasil Ditambah",
      });
      setTimeout(() => {
        dispatch({
          type: "TABS_PINDAH",
          payload: {
            tabs: "4",
            datas: res.data.data.user,
          },
        });
      }, 2000);
    });
  };
  function callback(key) {}
  return (
    <Fragment>
      {/* <ProfileContainer> */}
      <div className="pt-3">
        <div className="container mt-8">
          <Form {...layout} name="Biodata" onFinish={handleSave}>
            <div className="ml-5 text-xl font-semibold">Pengalaman Kerja</div>
            <Divider />
            <div className="grid grid-flow-col grid-cols-2 gap-4 flex justify-around">
              <Form.Item
                label={"Perusahaan:"}
                name="company_name"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Perusahaan",
                  },
                ]}
              >
                <Input placeholder="Masukkan Perusahaan" />
              </Form.Item>
              <Form.Item
                label={"Posisi:"}
                name="position"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Posisi",
                  },
                ]}
              >
                <Input placeholder="Masukkan Posisi" />
              </Form.Item>
            </div>
            <div className="grid grid-flow-col grid-cols-2 gap-4 flex justify-around">
              <Form.Item
                label={"Tanggal Mulai:"}
                name="starting_from"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Tanggal Mulai Kerja",
                  },
                ]}
              >
                <DatePicker format={"DD-MM-YYYY"} />
              </Form.Item>
              <Form.Item
                label={"Tanggal Terakhir:"}
                name="ending_in"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Tanggal Terakhir Kerja",
                  },
                ]}
              >
                <DatePicker format={"DD-MM-YYYY"} />
              </Form.Item>
            </div>
            <div className="text-right pr-5">
              <Button
                className="w-20 btnActive"
                style={{ backgroundColor: "#cf1322", color: "#FFF" }}
                htmlType="submit"
              >
                Simpan
              </Button>
            </div>
          </Form>
        </div>
      </div>
      {/* </ProfileContainer> */}
    </Fragment>
  );
};
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 12 },
};

export default Career;
