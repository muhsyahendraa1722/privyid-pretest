import {
  Button,
  DatePicker,
  Divider,
  Form,
  Input,
  notification,
  Select,
} from "antd";
import moment from "moment";
import React, { Fragment, useContext, useState } from "react";
import { ApiContext } from "../../context";
import { postData } from "../../utils/fetchData";

const { Option } = Select;
const { TextArea } = Input;
const Profile = (props) => {
  const apiContext = useContext(ApiContext);
  const { state, dispatch } = apiContext;
  const handleSave = (e) => {
    // props.setStateTabs("1");
    let datePick = moment(e.birthday).format("DD-MM-YYYY");
    let access_token = sessionStorage.getItem("token");
    let datas = new FormData();
    datas.append("name", e.nama);
    datas.append("birthday", datePick);
    datas.append("gender", e.gender);
    datas.append("hometown", e.hometown);
    datas.append("bio", e.bio);
    postData("/api/v1/profile", datas, access_token).then((res) => {
      notification["success"]({
        message: "Sukses",
        description: "Berhasil Ditambah",
      });
      setTimeout(() => {
        dispatch({
          type: "TABS_PINDAH",
          payload: {
            tabs: "2",
            datas: res.data.data.user,
          },
        });
      }, 2000);
    });
  };
  return (
    <Fragment>
      {/* <ProfileContainer> */}
      <div className="pt-3">
        <div className="container mt-8">
          <Form {...layout} name="Biodata" onFinish={handleSave}>
            <div className="ml-5 text-xl font-semibold">Profile</div>
            <Divider />
            <div className="grid grid-flow-col grid-cols-2 gap-4 flex justify-around">
              <Form.Item
                label={"Nama:"}
                name="nama"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Nama",
                  },
                ]}
              >
                <Input placeholder="Masukkan Nama" />
              </Form.Item>
              <Form.Item
                label={"Jenis Kelamin:"}
                name="gender"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Jenis Kelamin",
                  },
                ]}
              >
                <Select placeholder="Pilih" style={{ width: "100%" }}>
                  <Option value="0">Laki-Laki</Option>
                  <Option value="1">Perempuan</Option>
                </Select>
              </Form.Item>
            </div>
            <div className="grid grid-flow-col grid-cols-2 md:grid-cols-2 gap-4 justify-around">
              <Form.Item
                label={"Tanggal Lahir:"}
                name="birthday"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Tanggal Lahir",
                  },
                ]}
              >
                <DatePicker format={"DD-MM-YYYY"} />
              </Form.Item>
              <Form.Item
                label={"Alamat:"}
                name="hometown"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Alamat",
                  },
                ]}
              >
                <Input placeholder="Masukkan Alamat" />
              </Form.Item>
            </div>
            <div className="grid grid-flow-col grid-cols-2 md:grid-cols-2 gap-4">
              <Form.Item
                label={"Biodata:"}
                name="bio"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Biodata",
                  },
                ]}
              >
                <TextArea rows={4} />
              </Form.Item>
            </div>
            <div className="text-right pr-5">
              <Button
                className="w-20 btnActive"
                style={{ backgroundColor: "#cf1322", color: "#FFF" }}
                htmlType="submit"
              >
                Simpan
              </Button>
            </div>
          </Form>
        </div>
      </div>
      {/* </ProfileContainer> */}
    </Fragment>
  );
};
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 12 },
};

export default Profile;
