import {
  Button,
  DatePicker,
  Divider,
  Form,
  Input,
  notification,
  Select,
} from "antd";
import moment from "moment";
import React, { Fragment, useContext, useState } from "react";
import { ApiContext } from "../../context";
import { postData } from "../../utils/fetchData";

const Education = (props) => {
  const apiContext = useContext(ApiContext);
  const { state, dispatch } = apiContext;
  const handleSave = (e) => {
    // props.setStateTabs("1");
    let datePick = moment(e.birthday).format("DD-MM-YYYY");
    let access_token = sessionStorage.getItem("token");
    let datas = new FormData();
    datas.append("school_name", e.school_name);
    datas.append("graduation_time", datePick);
    postData("/api/v1/profile/education", datas, access_token).then((res) => {
      notification["success"]({
        message: "Sukses",
        description: "Berhasil Ditambah",
      });
      setTimeout(() => {
        dispatch({
          type: "TABS_PINDAH",
          payload: {
            tabs: "3",
            datas: res.data.data.user,
          },
        });
      }, 2000);
    });
  };
  return (
    <Fragment>
      {/* <ProfileContainer> */}
      <div className="pt-3">
        <div className="container mt-8">
          <Form {...layout} name="Biodata" onFinish={handleSave}>
            <div className="ml-5 text-xl font-semibold">Pendidikan</div>
            <Divider />
            <div className="grid grid-flow-col grid-cols-2 gap-4 flex justify-around">
              <Form.Item
                label={"Sekolah:"}
                name="school_name"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Sekolah",
                  },
                ]}
              >
                <Input placeholder="Masukkan Sekolah" />
              </Form.Item>
              <Form.Item
                label={"Tanggal Lulus:"}
                name="graduation_time"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Tanggal Lulus",
                  },
                ]}
              >
                <DatePicker format={"DD-MM-YYYY"} />
              </Form.Item>
            </div>
            <div className="text-right pr-5">
              <Button
                className="w-20 btnActive"
                style={{ backgroundColor: "#cf1322", color: "#FFF" }}
                htmlType="submit"
              >
                Simpan
              </Button>
            </div>
          </Form>
        </div>
      </div>
      {/* </ProfileContainer> */}
    </Fragment>
  );
};
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 12 },
};

export default Education;
