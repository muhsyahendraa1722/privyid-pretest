import { Button, Input, Form, notification } from "antd";
import { Fragment, useState } from "react";
import { useHistory } from "react-router-dom";
import LoginRegisterContainer from "../../container/loginRegisterContainer";
import { postData } from "../../utils/fetchData";

const LoginRegister = (props) => {
  const [stateLoading, setStateLoading] = useState(false);
  const history = useHistory();
  const login = async (e) => {
    let result = new FormData();
    result.append("phone", e.NoHP);
    result.append("password", e.password);
    // result.append("password", e.password);
    result.append("country", "INDONESIA");
    result.append("latlong", "-62");
    result.append("device_token", String(Math.random() * 10));
    result.append("device_type", 2);
    setStateLoading(true);
    postData("/api/v1/oauth/sign_in", result)
      .then((res) => {
        const resData = res.data.data;
        sessionStorage.setItem("token", resData.user.access_token);
        setStateLoading(false);
        history.push("/myprofile");
        notification["success"]({
          message: "Sukses",
          description: "Berhasil Login",
        });
      })
      .catch((e) => {
        const res = e.response;
        console.log(res, "inires");
        setStateLoading(false);
        res?.data?.error.errors.map((item) => {
          notification["error"]({
            message: "Error",
            description: item.message ? item.message : item,
          });
        });
      });
  };
  const register = async (e) => {
    setStateLoading(true);
    try {
      let result = new FormData();
      result.append("phone", e.NoHP);
      result.append("password", e.password);
      // result.append("password", e.password);
      result.append("country", "INDONESIA");
      result.append("latlong", "-62");
      result.append("device_token", String(Math.random() * 10));
      result.append("device_type", 2);
      setStateLoading(true);
      const id = document.getElementById("id");
      if (e.password === e.password1) {
        const { data } = await postData("/api/v1/register", result);
        const res = data.data;
        if (res.user.phone) {
          let userPhoneForm = new FormData();
          userPhoneForm.append("phone", res.user.phone);
          postData("/api/v1/register/otp/request", userPhoneForm)
            .then((result) => {
              history.push({
                pathname: "/otp/" + res.user.id,
                state: { condition: true, user: res.user.phone },
              });
              notification["success"]({
                message: "Sukses",
                description:
                  "Kode OTP Berhasil dikirim ke nomor" +
                  result.data?.data?.user.phone,
              });
            })
            .catch((e) => {
              const res = e.response;
              res.data?.error?.errors.map((item) => {
                return notification["error"]({
                  message: "Error",
                  description: item,
                });
              });
            });
          setStateLoading(false);
        }
      } else {
        id.innerHTML = "Masukkan Password Yang Sama";
        setTimeout(() => {
          id.style.display = "none";
        }, 3000);
        setStateLoading(false);
      }
    } catch (e) {
      const res = e.response;
      setStateLoading(false);
      res?.data?.error.errors.map((item) => {
        return notification["error"]({
          message: "Error",
          description: item.message ? item.message : item,
        });
      });
    }
  };

  return (
    <LoginRegisterContainer>
      {window.location.pathname === "/login" ? (
        <>
          <div className="text-center text-lg mt-3">
            Selamat Datang di PrivyID
          </div>

          <div className="text-center text-lg mt-3 font-semibold  text-red-600">
            Log In
          </div>
          <Form
            {...layout}
            name="Login"
            onFinish={login}
            // initialValues={{ remember: true }}
            // onFinish={onFinish}
            // onFinishFailed={onFinishFailed}
          >
            <div className="mt-14 flex">
              <Form.Item
                label={null}
                name="NoHP"
                rules={[
                  {
                    required: true,
                    message: "Masukkan No. Telp",
                  },
                ]}
              >
                <div className="w-56">
                  <Input placeholder="Masukkan Nomer HP" />
                </div>
              </Form.Item>
            </div>
            <div>
              <Form.Item
                label={null}
                name="password"
                rules={[
                  {
                    required: true,
                    message: "Masukkan Password",
                  },
                ]}
              >
                <div className="w-56">
                  <Input type="password" placeholder="Masukkan Password" />
                </div>
              </Form.Item>
            </div>
            <div className="flex justify-center">
              <Form.Item>
                <Button
                  htmlType="submit"
                  className="w-40 btnActive"
                  style={{
                    backgroundColor: stateLoading ? "#ff7875" : "#f5222d",
                    color: "white",
                    borderRadius: 10,
                    height: 35,
                  }}
                  disabled={stateLoading}
                >
                  {stateLoading ? "Loading.." : "Login"}
                </Button>
              </Form.Item>
            </div>
            <div>
              <div className="mt-3 text-center font-semibold">
                Belum Punya Akun ?{" "}
                <span
                  className="underline font-normal text-blue-500 cursor-pointer"
                  onClick={() => history.push("/register")}
                >
                  Daftar
                </span>
              </div>
            </div>
          </Form>
        </>
      ) : (
        <>
          <div className="text-center text-lg mt-3">Pendaftaran Akun</div>
          <Form {...layout} name="Register" onFinish={register}>
            <Form.Item
              name="NoHP"
              rules={[
                {
                  required: true,
                  message: "Masukkan Nomor Telepon",
                },
              ]}
            >
              <div className="mt-20 w-56">
                <div>Nomor Telepon:</div>
                <Input placeholder="Masukkan Nomer Telepon" />
              </div>
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: "Masukkan Password",
                },
              ]}
            >
              <div className="w-56">
                <div>Password:</div>
                <Input
                  id={"Pass1"}
                  type="password"
                  placeholder="Masukkan Password"
                />
              </div>
            </Form.Item>
            <Form.Item
              name="password1"
              rules={[
                {
                  required: true,
                  message: "Masukkan Password Kembali",
                },
              ]}
            >
              <div className="w-56">
                <div>Ulangi Password:</div>
                <Input
                  type="password"
                  id={"Pass2"}
                  placeholder="Masukkan Password Kembali"
                />
                <div id={"id"}></div>
              </div>
            </Form.Item>
            <div className="flex justify-center w-56">
              <Form.Item>
                <Button
                  htmlType="submit"
                  className="w-40 btnActive"
                  disabled={stateLoading}
                  style={{
                    backgroundColor: stateLoading ? "#ff7875" : "#f5222d",
                    color: "white",
                    borderRadius: 10,
                    height: 35,
                  }}
                >
                  {stateLoading ? "Loading.." : "Daftar"}
                </Button>
              </Form.Item>
            </div>
            <div className="mt-3 text-center font-semibold">
              Sudah Punya Akun ?{" "}
              <span
                className="underline font-normal text-blue-500 cursor-pointer"
                onClick={() => history.push("/login")}
              >
                Login
              </span>
            </div>
          </Form>
        </>
      )}
    </LoginRegisterContainer>
  );
};

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 24 },
};

export default LoginRegister;
