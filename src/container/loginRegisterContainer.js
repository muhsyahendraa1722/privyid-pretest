import React, { Fragment } from "react";

const LoginRegisterContainer = (props) => {
  return (
    <Fragment>
      <div className="flex justify-center">
        <div
          className="bg-white p-2 shadow-2xl sm:w-1/3 lg:w-1/3  absolute  flex justify-center bg-red"
          style={{ height: "100vh" }}
        >
          <div className="block">{props.children}</div>
        </div>
      </div>
    </Fragment>
  );
};
export default LoginRegisterContainer;
