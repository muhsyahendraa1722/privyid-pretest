import React, { Fragment } from "react";
import "./style.css";

const ProfileContainer = (props) => {
  return (
    <Fragment>
      <div className=" flex justify-center">
        <div className="profileContainer shadow-md w-10/12">
          <div>{props.children}</div>
        </div>
      </div>
    </Fragment>
  );
};

export default ProfileContainer;
