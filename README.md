# Pretest Privi-ID


## Pendahuluan

React adalah pustaka/library yang bersifat opensource yang dibuat oleh tim pengembang Facebook. Dalam hal mendesain sebuah web react dapat diimplementasikan untuk membuat web sederhana maupun yang lebih profesional, didukung dengan banyak library react membuat semakin populer dikalangan programmer. dan mudah digunakan untuk orang yang baru mulai masuk didunia IT atau programmer.
Aplikasi didukung dengan bantuan node sehingga mempunyai server sendiri untuk menjalankan sebuah website localhost. sehingga dapat mempermudah tim developer untuk mengembangkan sebuah Aplikasi

## Cara Menjalankan
Cara menjalan program ini:
1. install node dan npm
2. clone repo ini https://gitlab.com/muhsyahendraa1722/privyid-pretest.git
3. cd privyid-pretest
4. npm install
5. npm start

Akan diarahkan ke halaman login. jika belum mempunyai sebuah akun dapat membuat akun dengan mengklik tulisan 'daftar'
setelah itu diarahkan ke form halaman register. isi semua form. setelah itu masukan kode OTP yang tadi diregistrasi cek inbox sms. setelah itu akan diarahkan pengisian form biodata, karir, sekolah dan upload foto profile. Klik salah satu foto profile nanti akan menjadi default dari foto profile. setelah semua selesai nanti halaman page akan reload untuk memvalidasi biodata itu. selamat akun anda berhasil dibuat.


## License
dibuat oleh Muh Syahendra Anindyantoro
